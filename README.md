# Frameworks

The frameworks DewSystems uses and plays with.
There are no changes done to the frameworks. Maybe only an extra controller or class created.
But the main framework and skeletons / barebones remained the same as when they would be installed using Composer.

## License

This software is not a product of DewSystems and is thereby licensed under their own licenses.
For more details, refer to [the LICENSE.md file](LICENSE.md).
