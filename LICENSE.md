# License

This is a collection of software that is licensed themselves. Thus, each subdirectory of this repository should contain, somewhere, a note about the license. For more details, refer to those.

## MIT

Most of the frameworks are licensed under the [MIT license](https://opensource.org/licenses/MIT).